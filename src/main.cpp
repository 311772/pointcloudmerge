#include "main.h"

#include <iostream>
#include <cloud.h>

int32_t main(void) {
    Cloud cl(10);

    cl.AddPoint(10.0, 11.0, 12.0);
    cl.AddPoint(10.0, 11.0, 12.0);
    cl.AddPoint(10.0, 11.0, 12.0);
    cl.Print();

    return 0;
}

