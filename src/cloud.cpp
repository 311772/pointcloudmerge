#include <cloud.h>

template <typename T>
Point<T>::Point(T x, T y, T z)
    :   _x(x),
        _y(y),
        _z(z)
{ }

Cloud Cloud::operator+(const Cloud& other)
{ return Cloud(other); }

void Cloud::Print() const
{
    std::cout << "Points in coud: " << _cloud.size() << std::endl;
    for (Cloud::CloudPointPtr n : _cloud)
        n->Print();
    std::cout << std::endl;
}

template class Point<POINT_CLOUD_TYPE>;
