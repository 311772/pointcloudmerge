#ifndef CLOUD_H
#define CLOUD_H

#include <vector>
#include <memory>
#include <iostream>

#define POINT_CLOUD_TYPE float

template <typename T>
class Point
{
	T _x;
	T _y;
	T _z;

public:
	Point(T x, T y, T z);

	Point<T> operator+(const Point<T>& other)
	{ return Point<T>(_x + other._x, _y + other._y, _z + other._z); }

	void Print() const
	{ std::cout << _x << " " << _y << " " << _z << std::endl; }
};

class Cloud
{
	using CloudPoint = Point<POINT_CLOUD_TYPE>;
	using CloudPointPtr = std::shared_ptr<CloudPoint>;

	std::vector<CloudPointPtr> _cloud;

public:
	explicit Cloud(size_t size)
	{ _cloud.reserve(size); }

	void AddPoint(const CloudPointPtr& point)
	{ _cloud.push_back(point); }

	void AddPoint(const POINT_CLOUD_TYPE x, const POINT_CLOUD_TYPE y, const POINT_CLOUD_TYPE z)
	{ _cloud.push_back(std::make_shared<CloudPoint>(x, y, z)); }

	Cloud operator+(const Cloud& other);

	void Print() const;
};

#endif
